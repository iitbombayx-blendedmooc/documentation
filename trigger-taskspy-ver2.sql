-- MySQL dump 10.13  Distrib 5.5.37, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: edxapp
-- ------------------------------------------------------
-- Server version	5.5.37-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `auth_group$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER auth_group$insert AFTER INSERT ON auth_group FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'name': '", NEW.name, "'" );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "auth_group", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `auth_group$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER auth_group$update AFTER UPDATE ON auth_group FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 


        IF NEW.name <> OLD.name THEN
            set message = concat( message, ",'name': '", NEW.name, "'" ); 
        END IF;


        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "auth_group", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `auth_group$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER auth_group$delete AFTER DELETE ON auth_group FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'name': '", OLD.name, "'" ); 
        set message = concat( message, "}" );

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "auth_group", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `auth_user$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER auth_user$insert AFTER INSERT ON auth_user FOR EACH ROW
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );

        set message = concat( message, ",'username': '", NEW.username, "'" ); 
        set message = concat( message, ",'first_name': '", NEW.first_name, "'" ); 
        set message = concat( message, ",'last_name': '", NEW.last_name, "'" ); 
        set message = concat( message, ",'email': '", NEW.email, "'" ); 
        set message = concat( message, ",'password': '", NEW.password, "'" ); 
        set message = concat( message, ",'is_staff': ", NEW.is_staff, "" ); 
        set message = concat( message, ",'is_active': ", NEW.is_active, "" ); 
        set message = concat( message, ",'is_superuser': ", NEW.is_superuser, "" ); 
        set message = concat( message, ",'last_login': '", NEW.last_login, "'" ); 
        set message = concat( message, ",'date_joined': '", NEW.date_joined, "'" ); 
        set message = concat( message, "}" ); 
        
        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "auth_user", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `auth_user$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER auth_user$update AFTER UPDATE ON auth_user FOR EACH ROW
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 

        IF NEW.username <> OLD.username THEN
            set message = concat( message, ",'username': '", NEW.username, "'" ); 
        END IF;

        IF NEW.first_name <> OLD.first_name THEN
            set message = concat( message, ",'first_name': '", NEW.first_name, "'" ); 
        END IF;

        IF NEW.last_name <> OLD.last_name THEN
            set message = concat( message, ",'last_name': '", NEW.last_name, "'" ); 
        END IF;

        IF NEW.email <> OLD.email THEN
            set message = concat( message, ",'email': '", NEW.email, "'" ); 
        END IF;

        IF NEW.password <> OLD.password THEN
            set message = concat( message, ",'password': '", NEW.password, "'" ); 
        END IF;

        IF NEW.is_staff <> OLD.is_staff THEN
            set message = concat( message, ",'is_staff': ", NEW.is_staff, "" ); 
        END IF;


        IF NEW.is_active <> OLD.is_active THEN
            set message = concat( message, ",'is_active': ", NEW.is_active, "" ); 
        END IF;

        IF NEW.is_superuser <> OLD.is_superuser THEN
            set message = concat( message, ",'is_superuser': ", NEW.is_superuser, "" ); 
        END IF;

        IF NEW.last_login <> OLD.last_login THEN
            set message = concat( message, ",'last_login': '", NEW.last_login, "'" ); 
        END IF;

        IF NEW.date_joined <> OLD.date_joined THEN
            set message = concat( message, ",'date_joined': '", NEW.date_joined, "'" ); 
        END IF;

        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "auth_user", "UPDATE", NEW.id, message); 
        END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `auth_user$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER auth_user$delete AFTER DELETE ON auth_user FOR EACH ROW
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );

        set message = concat( message, ",'username': '", OLD.username, "'" ); 
        set message = concat( message, ",'first_name': '", OLD.first_name, "'" ); 
        set message = concat( message, ",'last_name': '", OLD.last_name, "'" ); 
        set message = concat( message, ",'email': '", OLD.email, "'" ); 
        set message = concat( message, ",'password': '", OLD.password, "'" ); 
        set message = concat( message, ",'is_staff': ", OLD.is_staff, "" ); 
        set message = concat( message, ",'is_active': ", OLD.is_active, "" ); 
        set message = concat( message, ",'is_superuser': ", OLD.is_superuser, "" ); 
        set message = concat( message, ",'last_login': '", OLD.last_login, "'" ); 
        set message = concat( message, ",'date_joined': '", OLD.date_joined, "'" ); 
        set message = concat( message, "}" ); 
        
        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "auth_user", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `auth_userprofile$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER auth_userprofile$insert AFTER INSERT ON auth_userprofile FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'user_id': ", NEW.user_id, "" ); 
        set message = concat( message, ",'name': '", NEW.name, "'" ); 
        set message = concat( message, ",'meta': '''", NEW.meta, "'''" ); 
        set message = concat( message, ",'courseware': '", NEW.courseware, "'" ); 
        set message = concat( message, ",'language': '", NEW.language, "'" ); 
        set message = concat( message, ",'location': '", NEW.location, "'" ); 
        set message = concat( message, ",'year_of_birth': ", IFNULL(NEW.year_of_birth, '''NULL'''), "" ); 
        set message = concat( message, ",'gender': '''", IFNULL(NEW.gender, 'NULL'), "'''" ); 
        set message = concat( message, ",'level_of_education': '''", IFNULL(NEW.level_of_education, 'NULL'), "'''" ); 
        set message = concat( message, ",'mailing_address': '''", IFNULL(NEW.mailing_address, 'NULL'), "'''" ); 
        set message = concat( message, ",'city': '''", IFNULL(NEW.city, 'NULL'), "'''" ); 
        set message = concat( message, ",'country': '''", IFNULL(NEW.country, 'NULL'), "'''" ); 
        set message = concat( message, ",'goals': '''", IFNULL(NEW.goals, 'NULL'), "'''" ); 
        set message = concat( message, ",'allow_certificate': ", NEW.allow_certificate, "" ); 
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "auth_userprofile", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `auth_userprofile$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER auth_userprofile$update AFTER UPDATE ON auth_userprofile FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 

        IF NEW.user_id <> OLD.user_id THEN
            set message = concat( message, ",'user_id': ", NEW.user_id, "" ); 
        END IF;

        IF NEW.name <> OLD.name THEN
            set message = concat( message, ",'name': '", NEW.name, "'" ); 
        END IF;

        IF NEW.meta <> OLD.meta THEN
            set message = concat( message, ",'meta': '''", NEW.meta, "'''" ); 
        END IF;

        IF NEW.courseware <> OLD.courseware THEN
            set message = concat( message, ",'courseware': '", NEW.courseware, "'" ); 
        END IF;

        IF NEW.language <> OLD.language THEN
            set message = concat( message, ",'language': '", NEW.language, "'" ); 
        END IF;

        IF NEW.location <> OLD.location THEN
            set message = concat( message, ",'location': '", NEW.location, "'" ); 
        END IF;

        IF ( (OLD.year_of_birth IS NOT NULL AND NEW.year_of_birth IS NOT NULL 
               AND NEW.year_of_birth <> OLD.year_of_birth) 
               OR (NEW.year_of_birth IS NULL AND OLD.year_of_birth IS NOT NULL) 
               OR (NEW.year_of_birth IS NOT NULL <> OLD.year_of_birth IS NULL) ) THEN
            set message = concat( message, ",'year_of_birth': ", IFNULL(NEW.year_of_birth, '''NULL'''), "" ); 
        END IF;

        IF ( (OLD.gender IS NOT NULL AND NEW.gender IS NOT NULL 
               AND NEW.gender <> OLD.gender) 
               OR (NEW.gender IS NULL AND OLD.gender IS NOT NULL) 
               OR (NEW.gender IS NOT NULL <> OLD.gender IS NULL) ) THEN
            set message = concat( message, ",'gender': '''", IFNULL(NEW.gender, 'NULL'), "'''" ); 
        END IF;

        IF ( (OLD.level_of_education IS NOT NULL AND NEW.level_of_education IS NOT NULL 
               AND NEW.level_of_education <> OLD.level_of_education) 
               OR (NEW.level_of_education IS NULL AND OLD.level_of_education IS NOT NULL) 
               OR (NEW.level_of_education IS NOT NULL <> OLD.level_of_education IS NULL) ) THEN
            set message = concat( message, ",'level_of_education': '''", IFNULL(NEW.level_of_education, 'NULL'), "'''" ); 
        END IF;

        IF ( (OLD.mailing_address IS NOT NULL AND NEW.mailing_address IS NOT NULL 
               AND NEW.mailing_address <> OLD.mailing_address) 
               OR (NEW.mailing_address IS NULL AND OLD.mailing_address IS NOT NULL) 
               OR (NEW.mailing_address IS NOT NULL <> OLD.mailing_address IS NULL) ) THEN
            set message = concat( message, ",'mailing_address': '''", IFNULL(NEW.mailing_address, 'NULL'), "'''" ); 
        END IF;

        IF ( (OLD.city IS NOT NULL AND NEW.city IS NOT NULL 
               AND NEW.city <> OLD.city) 
               OR (NEW.city IS NULL AND OLD.city IS NOT NULL) 
               OR (NEW.city IS NOT NULL <> OLD.city IS NULL) ) THEN
            set message = concat( message, ",'city': '''", IFNULL(NEW.city, 'NULL'), "'''" ); 
        END IF;

        IF ( (OLD.country IS NOT NULL AND NEW.country IS NOT NULL 
               AND NEW.country <> OLD.country) 
               OR (NEW.country IS NULL AND OLD.country IS NOT NULL) 
               OR (NEW.country IS NOT NULL <> OLD.country IS NULL) ) THEN
            set message = concat( message, ",'country': '''", IFNULL(NEW.country, 'NULL'), "'''" ); 
        END IF;

        IF ( (OLD.goals IS NOT NULL AND NEW.goals IS NOT NULL 
               AND NEW.goals <> OLD.goals) 
               OR (NEW.goals IS NULL AND OLD.goals IS NOT NULL) 
               OR (NEW.goals IS NOT NULL <> OLD.goals IS NULL) ) THEN
            set message = concat( message, ",'goals': '''", IFNULL(NEW.goals, 'NULL'), "'''" ); 
        END IF;

        IF NEW.allow_certificate <> OLD.allow_certificate THEN
            set message = concat( message, ",'allow_certificate': ", NEW.allow_certificate, "" ); 
        END IF;


        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "auth_userprofile", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `auth_userprofile$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER auth_userprofile$delete AFTER DELETE ON auth_userprofile FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'user_id': ", OLD.user_id, "" ); 
        set message = concat( message, ",'name': '", OLD.name, "'" ); 
        set message = concat( message, ",'meta': '''", OLD.meta, "'''" ); 
        set message = concat( message, ",'courseware': '", OLD.courseware, "'" ); 
        set message = concat( message, ",'language': '", OLD.language, "'" ); 
        set message = concat( message, ",'location': '", OLD.location, "'" ); 
        set message = concat( message, ",'year_of_birth': ", IFNULL(OLD.year_of_birth, '''NULL'''), "" ); 
        set message = concat( message, ",'gender': '''", IFNULL(OLD.gender, 'NULL'), "'''" ); 
        set message = concat( message, ",'level_of_education': '''", IFNULL(OLD.level_of_education, 'NULL'), "'''" ); 
        set message = concat( message, ",'mailing_address': '''", IFNULL(OLD.mailing_address, 'NULL'), "'''" ); 
        set message = concat( message, ",'city': '''", IFNULL(OLD.city, 'NULL'), "'''" ); 
        set message = concat( message, ",'country': '''", IFNULL(OLD.country, 'NULL'), "'''" ); 
        set message = concat( message, ",'goals': '''", IFNULL(OLD.goals, 'NULL'), "'''" ); 
        set message = concat( message, ",'allow_certificate': ", OLD.allow_certificate, "" ); 
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "auth_userprofile", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `auth_user_groups$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER auth_user_groups$insert AFTER INSERT ON auth_user_groups FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'user_id': ", NEW.user_id, "" ); 
        set message = concat( message, ",'group_id': ", NEW.group_id, "" ); 
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "auth_user_groups", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `auth_user_groups$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER auth_user_groups$update AFTER UPDATE ON auth_user_groups FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 


        IF NEW.user_id <> OLD.user_id THEN
            set message = concat( message, ",'user_id': ", NEW.user_id, "" ); 
        END IF;

        IF NEW.group_id <> OLD.group_id THEN
            set message = concat( message, ",'group_id': ", NEW.group_id, "" ); 
        END IF;

        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "auth_user_groups", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `auth_user_groups$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER auth_user_groups$delete AFTER DELETE ON auth_user_groups FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'user_id': ", OLD.user_id, "" ); 
        set message = concat( message, ",'group_id': ", OLD.group_id, "" ); 
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "auth_user_groups", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_person$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_person$insert AFTER INSERT ON mooc_person FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'user_id': ", NEW.user_id, "" ); 
        set message = concat( message, ",'birth_date': '''", IFNULL(NEW.birth_date, 'NULL'), "'''" ); 
        set message = concat( message, ",'mobile': ", IFNULL(NEW.mobile, '''NULL'''), "" ); 
        set message = concat( message, ",'country_code': '''", IFNULL(NEW.country_code, 'NULL'), "'''" ); 
        set message = concat( message, "}" ); 
        
        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_person", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_person$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_person$update AFTER UPDATE ON mooc_person FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 


        IF NEW.user_id <> OLD.user_id THEN
            set message = concat( message, ",'user_id': ", NEW.user_id, "" ); 
        END IF;

        IF ( (OLD.birth_date IS NOT NULL AND NEW.birth_date IS NOT NULL 
               AND NEW.birth_date <> OLD.birth_date) 
               OR (NEW.birth_date IS NULL AND OLD.birth_date IS NOT NULL) 
               OR (NEW.birth_date IS NOT NULL <> OLD.birth_date IS NULL) ) THEN
            set message = concat( message, ",'birth_date': '''", IFNULL(NEW.birth_date, 'NULL'), "'''" ); 
        END IF;

        IF ( (OLD.mobile IS NOT NULL AND NEW.mobile IS NOT NULL 
               AND NEW.mobile <> OLD.mobile) 
               OR (NEW.mobile IS NULL AND OLD.mobile IS NOT NULL) 
               OR (NEW.mobile IS NOT NULL <> OLD.mobile IS NULL) ) THEN
            set message = concat( message, ",'mobile': ", IFNULL(NEW.mobile, '''NULL'''), "" ); 
        END IF;

        IF ( (OLD.country_code IS NOT NULL AND NEW.country_code IS NOT NULL 
               AND NEW.country_code <> OLD.country_code) 
               OR (NEW.country_code IS NULL AND OLD.country_code IS NOT NULL) 
               OR (NEW.country_code IS NOT NULL <> OLD.country_code IS NULL) ) THEN
            set message = concat( message, ",'country_code': '''", IFNULL(NEW.country_code, 'NULL'), "'''" ); 
        END IF;

        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_person", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_person$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_person$delete AFTER DELETE ON mooc_person FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'user_id': ", OLD.user_id, "" ); 
        set message = concat( message, ",'birth_date': '''", IFNULL(OLD.birth_date, 'NULL'), "'''" ); 
        set message = concat( message, ",'mobile': ", IFNULL(OLD.mobile, '''NULL'''), "" ); 
        set message = concat( message, ",'country_code': '''", IFNULL(OLD.country_code, 'NULL'), "'''" ); 
        set message = concat( message, "}" ); 
        
        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_person", "delete", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `courseware_studentmodule$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER courseware_studentmodule$insert AFTER INSERT ON courseware_studentmodule FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'module_type': '", NEW.module_type, "'" ); 
        set message = concat( message, ",'module_id': '", NEW.module_id, "'" ); 
        set message = concat( message, ",'student_id': ", NEW.student_id, "" ); 
        set message = concat( message, ",'state': '''", IFNULL(NEW.state, 'NULL'), "'''" ); 
        set message = concat( message, ",'grade': ", IFNULL(NEW.grade, '''NULL'''), "" ); 
        set message = concat( message, ",'created': '", NEW.created, "'" ); 
        set message = concat( message, ",'modified': '", NEW.modified, "'" ); 
        set message = concat( message, ",'max_grade': ", IFNULL(NEW.max_grade, '''NULL'''), "" ); 
        set message = concat( message, ",'done': '", NEW.done, "'"); 
        set message = concat( message, ",'course_id': '", NEW.course_id, "'" ); 
        set message = concat( message, "}" ); 
        
        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "courseware_studentmodule", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `courseware_studentmodule$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER courseware_studentmodule$update AFTER UPDATE ON courseware_studentmodule FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 

        IF NEW.module_type <> OLD.module_type THEN
            set message = concat( message, ",'module_type': '", NEW.module_type, "'" ); 
        END IF;

        IF NEW.module_id <> OLD.module_id THEN
            set message = concat( message, ",'module_id': '", NEW.module_id, "'" ); 
        END IF;

        IF NEW.student_id <> OLD.student_id THEN
            set message = concat( message, ",'student_id': ", NEW.student_id, "" ); 
        END IF;

        IF ( (OLD.state IS NOT NULL AND NEW.state IS NOT NULL 
               AND NEW.state <> OLD.state) 
               OR (NEW.state IS NULL AND OLD.state IS NOT NULL) 
               OR (NEW.state IS NOT NULL <> OLD.state IS NULL) ) THEN
            set message = concat( message, ",'state': '''", IFNULL(NEW.state, 'NULL'), "'''" ); 
        END IF;

        IF ( (OLD.grade IS NOT NULL AND NEW.grade IS NOT NULL 
               AND NEW.grade <> OLD.grade) 
               OR (NEW.grade IS NULL AND OLD.grade IS NOT NULL) 
               OR (NEW.grade IS NOT NULL <> OLD.grade IS NULL) ) THEN
            set message = concat( message, ",'grade': ", IFNULL(NEW.grade, '''NULL'''), "" ); 
        END IF;

        IF NEW.created <> OLD.created THEN
            set message = concat( message, ",'created': '", NEW.created, "'" ); 
        END IF;

        IF NEW.modified <> OLD.modified THEN
            set message = concat( message, ",'modified': '", NEW.modified, "'" ); 
        END IF;

        IF ( (OLD.max_grade IS NOT NULL AND NEW.max_grade IS NOT NULL 
               AND NEW.max_grade <> OLD.max_grade) 
               OR (NEW.max_grade IS NULL AND OLD.max_grade IS NOT NULL) 
               OR (NEW.max_grade IS NOT NULL <> OLD.max_grade IS NULL) ) THEN
            set message = concat( message, ",'max_grade': ", IFNULL(NEW.max_grade, '''NULL'''), "" ); 
        END IF;

        IF NEW.done <> OLD.done THEN
            set message = concat( message, ",'done': '", NEW.done, "'"); 
        END IF;

        IF NEW.course_id <> OLD.course_id THEN
            set message = concat( message, ",'course_id': '", NEW.course_id, "'" ); 
        END IF;

        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "courseware_studentmodule", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `courseware_studentmodule$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER courseware_studentmodule$delete AFTER DELETE ON courseware_studentmodule FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'module_type': '", OLD.module_type, "'" ); 
        set message = concat( message, ",'module_id': '", OLD.module_id, "'" ); 
        set message = concat( message, ",'student_id': ", OLD.student_id, "" ); 
        set message = concat( message, ",'state': '''", IFNULL(OLD.state, 'NULL'), "'''" ); 
        set message = concat( message, ",'grade': ", IFNULL(OLD.grade, '''NULL'''), "" ); 
        set message = concat( message, ",'created': '", OLD.created, "'" ); 
        set message = concat( message, ",'modified': '", OLD.modified, "'" ); 
        set message = concat( message, ",'max_grade': ", IFNULL(OLD.max_grade, '''NULL'''), "" ); 
        set message = concat( message, ",'done': '", OLD.done, "'"); 
        set message = concat( message, ",'course_id': '", OLD.course_id, "'" ); 
        set message = concat( message, "}" ); 
        
        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "courseware_studentmodule", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `courseware_studentmodulehistory$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER courseware_studentmodulehistory$insert AFTER INSERT ON courseware_studentmodulehistory FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'student_module_id': ", NEW.student_module_id, "" ); 
        set message = concat( message, ",'version': '''", IFNULL(NEW.version, 'NULL'), "'''" ); 
        set message = concat( message, ",'created': '", NEW.created, "'" ); 
        set message = concat( message, ",'state': '''", IFNULL(NEW.state, 'NULL'), "'''" );
        set message = concat( message, ",'grade': ", IFNULL(NEW.grade, '''NULL'''), "" );   
        set message = concat( message, ",'max_grade': ", IFNULL(NEW.max_grade, '''NULL'''), "" ); 
        set message = concat( message, "}" ); 
        
        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "courseware_studentmodulehistory", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `courseware_studentmodulehistory$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER courseware_studentmodulehistory$update AFTER UPDATE ON courseware_studentmodulehistory FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 


        IF NEW.student_module_id <> OLD.student_module_id THEN
            set message = concat( message, ",'student_module_id': ", NEW.student_module_id, "" ); 
        END IF;

        IF ( (OLD.version IS NOT NULL AND NEW.version IS NOT NULL 
               AND NEW.version <> OLD.version) 
               OR (NEW.version IS NULL AND OLD.version IS NOT NULL) 
               OR (NEW.version IS NOT NULL <> OLD.version IS NULL) ) THEN
            set message = concat( message, ",'version': '''", IFNULL(NEW.version, 'NULL'), "'''" ); 
        END IF;

        IF NEW.created <> OLD.created THEN
            set message = concat( message, ",'created': '", NEW.created, "'" ); 
        END IF;


        IF ( (OLD.state IS NOT NULL AND NEW.state IS NOT NULL 
               AND NEW.state <> OLD.state) 
               OR (NEW.state IS NULL AND OLD.state IS NOT NULL) 
               OR (NEW.state IS NOT NULL <> OLD.state IS NULL) ) THEN
            set message = concat( message, ",'state': '''", IFNULL(NEW.state, 'NULL'), "'''" ); 
        END IF;

        IF ( (OLD.grade IS NOT NULL AND NEW.grade IS NOT NULL 
               AND NEW.grade <> OLD.grade) 
               OR (NEW.grade IS NULL AND OLD.grade IS NOT NULL) 
               OR (NEW.grade IS NOT NULL <> OLD.grade IS NULL) ) THEN
            set message = concat( message, ",'grade': ", IFNULL(NEW.grade, '''NULL'''), "" ); 
        END IF;


        IF ( (OLD.max_grade IS NOT NULL AND NEW.max_grade IS NOT NULL 
               AND NEW.max_grade <> OLD.max_grade) 
               OR (NEW.max_grade IS NULL AND OLD.max_grade IS NOT NULL) 
               OR (NEW.max_grade IS NOT NULL <> OLD.max_grade IS NULL) ) THEN
            set message = concat( message, ",'max_grade': ", IFNULL(NEW.max_grade, '''NULL'''), "" ); 
        END IF;

        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "courseware_studentmodulehistory", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `courseware_studentmodulehistory$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER courseware_studentmodulehistory$delete AFTER DELETE ON courseware_studentmodulehistory FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'student_module_id': ", OLD.student_module_id, "" ); 
        set message = concat( message, ",'version': '''", IFNULL(OLD.version, 'NULL'), "'''" ); 
        set message = concat( message, ",'created': '", OLD.created, "'" ); 
        set message = concat( message, ",'state': '''", IFNULL(OLD.state, 'NULL'), "'''" ); 
        set message = concat( message, ",'grade': ", IFNULL(OLD.grade, '''NULL'''), "" ); 
        set message = concat( message, ",'max_grade': ", IFNULL(OLD.max_grade, '''NULL'''), "" ); 
        set message = concat( message, "}" ); 
        
        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "courseware_studentmodulehistory", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `django_comment_client_permission_roles$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER django_comment_client_permission_roles$insert AFTER INSERT ON django_comment_client_permission_roles FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'permission_id': '", NEW.permission_id, "'" );
        set message = concat( message, ",'role_id': ", NEW.role_id, "" ); 
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "django_comment_client_permission_roles", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `django_comment_client_permission_roles$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER django_comment_client_permission_roles$update AFTER UPDATE ON django_comment_client_permission_roles FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 


        IF NEW.permission_id <> OLD.permission_id THEN
            set message = concat( message, ",'permission_id': '", NEW.permission_id, "'" ); 
        END IF;

        IF NEW.role_id <> OLD.role_id THEN
            set message = concat( message, ",'role_id': ", NEW.role_id, "" ); 
        END IF;

        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "django_comment_client_permission_roles", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `django_comment_client_permission_roles$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER django_comment_client_permission_roles$delete AFTER DELETE ON django_comment_client_permission_roles FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'permission_id': '", OLD.permission_id, "'" ); 
        set message = concat( message, ",'role_id': ", OLD.role_id, "" ); 
        set message = concat( message, "}" );

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "django_comment_client_permission_roles", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `django_comment_client_role$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER django_comment_client_role$insert AFTER INSERT ON django_comment_client_role FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'name': '", NEW.name, "'" );
        set message = concat( message, ",'course_id': '", NEW.course_id, "'" ); 
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "django_comment_client_role", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `django_comment_client_role$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER django_comment_client_role$update AFTER UPDATE ON django_comment_client_role FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 

        IF NEW.name <> OLD.name THEN
            set message = concat( message, ",'name': '", NEW.name, "'" ); 
        END IF;

        IF NEW.course_id <> OLD.course_id THEN
            set message = concat( message, ",'course_id': '", NEW.course_id, "'" ); 
        END IF;        
        
        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "django_comment_client_role", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `django_comment_client_role$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER django_comment_client_role$delete AFTER DELETE ON django_comment_client_role FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'name': '", OLD.name, "'" ); 
        set message = concat( message, ",'course_id': '", OLD.course_id, "'" );
        set message = concat( message, "}" );

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "django_comment_client_permission_roles", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `django_comment_client_role_users$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER django_comment_client_role_users$insert AFTER INSERT ON django_comment_client_role_users FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'role_id': ", NEW.role_id, "" );
        set message = concat( message, ",'user_id': ", NEW.user_id, "" ); 
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "django_comment_client_role_users", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `django_comment_client_role_users$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER django_comment_client_role_users$update AFTER UPDATE ON django_comment_client_role_users FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 

        IF NEW.role_id <> OLD.role_id THEN
            set message = concat( message, ",'role_id': ", NEW.role_id, "" ); 
        END IF;

        IF NEW.user_id <> OLD.user_id THEN
            set message = concat( message, ",'user_id': ", NEW.user_id, "" ); 
        END IF;

        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "django_comment_client_role_users", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `django_comment_client_role_users$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER django_comment_client_role_users$delete AFTER DELETE ON django_comment_client_role_users FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'role_id': ", OLD.role_id, "" );
        set message = concat( message, ",'user_id': ", OLD.user_id, "" ); 
        set message = concat( message, "}" );
        
        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "django_comment_client_role_users", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `student_courseenrollment$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER student_courseenrollment$insert AFTER INSERT ON student_courseenrollment FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'user_id': ", NEW.user_id, "" ); 
        set message = concat( message, ",'course_id': '", NEW.course_id, "'" ); 
        set message = concat( message, ",'created': '''", IFNULL(NEW.created, 'NULL'), "'''" ); 
        set message = concat( message, ",'is_active': ", NEW.is_active, "" );
        set message = concat( message, ",'mode': '", NEW.mode, "'" ); 
        set message = concat( message, "}" );

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "student_courseenrollment", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `student_courseenrollment$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER student_courseenrollment$update AFTER UPDATE ON student_courseenrollment FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 


        IF NEW.user_id <> OLD.user_id THEN
            set message = concat( message, ",'user_id': ", NEW.user_id, "" ); 
        END IF;

        IF NEW.course_id <> OLD.course_id THEN
            set message = concat( message, ",'course_id': '", NEW.course_id, "'" ); 
        END IF;

        IF ( (OLD.created IS NOT NULL AND NEW.created IS NOT NULL 
               AND NEW.created <> OLD.created) 
               OR (NEW.created IS NULL AND OLD.created IS NOT NULL) 
               OR (NEW.created IS NOT NULL <> OLD.created IS NULL) ) THEN
            set message = concat( message, ",'created': '''", IFNULL(NEW.created, 'NULL'), "'''" ); 
        END IF;

        IF NEW.is_active <> OLD.is_active THEN
            set message = concat( message, ",'is_active': ", NEW.is_active, "" ); 
        END IF;

        IF NEW.user_id <> OLD.user_id THEN
            set message = concat( message, ",'mode': '", NEW.mode, "'" ); 
        END IF;


        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "student_courseenrollment", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `student_courseenrollment$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER student_courseenrollment$delete AFTER DELETE ON student_courseenrollment FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'user_id': ", OLD.user_id, "" ); 
        set message = concat( message, ",'course_id': '", OLD.course_id, "'" ); 
        set message = concat( message, ",'created': '''", IFNULL(OLD.created, 'NULL'), "'''" ); 
        set message = concat( message, ",'is_active': ", OLD.is_active, "" );
        set message = concat( message, ",'mode': '", OLD.mode, "'" ); 
        set message = concat( message, "}" );


        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "student_courseenrollment", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_course$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_course$insert AFTER INSERT ON mooc_course FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'name': '", NEW.name, "'" );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_course", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_course$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_course$update AFTER UPDATE ON mooc_course FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 


        IF NEW.name <> OLD.name THEN
            set message = concat( message, ",'name': '", NEW.name, "'" ); 
        END IF;


        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_course", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_course$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_course$delete AFTER DELETE ON mooc_course FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'name': '", OLD.name, "'" ); 
        set message = concat( message, "}" );

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_course", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_course_registration$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_course_registration$insert AFTER INSERT ON mooc_course_registration FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'user_id': ", NEW.user_id, "" ); 
        set message = concat( message, ",'institute_id': ", NEW.institute_id, "" );
        set message = concat( message, ",'course_id': ", NEW.course_id, "" );
        set message = concat( message, ",'status_id': ", NEW.status_id, "" );
        set message = concat( message, ",'role_id': ", NEW.role_id, "" );
        set message = concat( message, ",'is_approved': ", NEW.is_approved, "" );           
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_course_registration", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_course_registration$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_course_registration$update AFTER UPDATE ON mooc_course_registration FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 

        IF NEW.user_id <> OLD.user_id THEN
            set message = concat( message, ",'user_id': ", NEW.user_id, "" ); 
        END IF;

        IF NEW.institute_id <> OLD.institute_id THEN
            set message = concat( message, ",'institute_id': ", NEW.institute_id, "" ); 
        END IF;

        IF NEW.course_id <> OLD.course_id THEN
            set message = concat( message, ",'course_id': ", NEW.course_id, "" ); 
        END IF;

        IF NEW.status_id <> OLD.status_id THEN
            set message = concat( message, ",'status_id': ", NEW.status_id, "" ); 
        END IF;

        IF NEW.role_id <> OLD.role_id THEN
            set message = concat( message, ",'role_id': ", NEW.role_id, "" ); 
        END IF;

        IF NEW.is_approved <> OLD.is_approved THEN
            set message = concat( message, ",'is_approved': ", NEW.is_approved, "" );            
        END IF;

        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_course_registration", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_course_registration$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_course_registration$delete AFTER DELETE ON mooc_course_registration FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'user_id': ", OLD.user_id, "" ); 
        set message = concat( message, ",'institute_id': ", OLD.institute_id, "" );
        set message = concat( message, ",'course_id': ", OLD.course_id, "" );
        set message = concat( message, ",'status_id': ", OLD.status_id, "" );
        set message = concat( message, ",'role_id': ", OLD.role_id, "" );
        set message = concat( message, ",'is_approved': ", OLD.is_approved, "" );           
        set message = concat( message, "}" );
        
        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_course_registration", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_faculty_institute$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_faculty_institute$insert AFTER INSERT ON mooc_faculty_institute FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'user_id': ", NEW.user_id, "" ); 
        set message = concat( message, ",'institute_id': ", NEW.institute_id, "" );
        set message = concat( message, ",'status_id': ", NEW.status_id, "" );
        set message = concat( message, ",'course_id': ", NEW.course_id, "" );
                   
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_faculty_institute", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_faculty_institute$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_faculty_institute$update AFTER UPDATE ON mooc_faculty_institute FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 

        IF NEW.user_id <> OLD.user_id THEN
            set message = concat( message, ",'user_id': ", NEW.user_id, "" ); 
        END IF;

        IF NEW.institute_id <> OLD.institute_id THEN
            set message = concat( message, ",'institute_id': ", NEW.institute_id, "" ); 
        END IF;

        IF NEW.status_id <> OLD.status_id THEN
            set message = concat( message, ",'status_id': ", NEW.status_id, "" ); 
        END IF;

        IF NEW.course_id <> OLD.course_id THEN
            set message = concat( message, ",'course_id': ", NEW.course_id, "" ); 
        END IF;

        
        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_faculty_institute", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_faculty_institute$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_faculty_institute$delete AFTER DELETE ON mooc_faculty_institute FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'user_id': ", OLD.user_id, "" ); 
        set message = concat( message, ",'institute_id': ", OLD.institute_id, "" );
        set message = concat( message, ",'status_id': ", OLD.status_id, "" );
        set message = concat( message, ",'course_id': ", OLD.course_id, "" );
        set message = concat( message, "}" );
        
        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_faculty_institute", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_institute_course$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_institute_course$insert AFTER INSERT ON mooc_institute_course FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'course_id': ", NEW.course_id, "" );
        set message = concat( message, ",'institute_id': ", NEW.institute_id, "" );
        set message = concat( message, ",'is_approved': ", NEW.is_approved, "" );           
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_institute_course", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_institute_course$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_institute_course$update AFTER UPDATE ON mooc_institute_course FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 


        IF NEW.course_id <> OLD.course_id THEN
            set message = concat( message, ",'course_id': ", NEW.course_id, "" ); 
        END IF;
        
        IF NEW.institute_id <> OLD.institute_id THEN
            set message = concat( message, ",'institute_id': ", NEW.institute_id, "" ); 
        END IF;

        IF NEW.is_approved <> OLD.is_approved THEN
            set message = concat( message, ",'is_approved': ", NEW.is_approved, "" );            
        END IF;

        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_institute_course", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_institute_course$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_institute_course$delete AFTER DELETE ON mooc_institute_course FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'course_id': ", OLD.course_id, "" );
        set message = concat( message, ",'institute_id': ", OLD.institute_id, "" );
        set message = concat( message, ",'is_approved': ", OLD.is_approved, "" );           
        set message = concat( message, "}" );
        
        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_institute_course", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_institute_designation$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_institute_designation$insert AFTER INSERT ON mooc_institute_designation FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'user_id': ", NEW.user_id, "" ); 
        set message = concat( message, ",'institute_id': ", NEW.institute_id, "" );
        set message = concat( message, ",'role_id': ", NEW.role_id, "" );
        set message = concat( message, ",'is_approved': ", NEW.is_approved, "" );           
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_institute_designation", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_institute_designation$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_institute_designation$update AFTER UPDATE ON mooc_institute_designation FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 

        IF NEW.user_id <> OLD.user_id THEN
            set message = concat( message, ",'user_id': ", NEW.user_id, "" ); 
        END IF;

        IF NEW.institute_id <> OLD.institute_id THEN
            set message = concat( message, ",'institute_id': ", NEW.institute_id, "" ); 
        END IF;

        IF NEW.role_id <> OLD.role_id THEN
            set message = concat( message, ",'role_id': ", NEW.role_id, "" ); 
        END IF;

        IF NEW.is_approved <> OLD.is_approved THEN
            set message = concat( message, ",'is_approved': ", NEW.is_approved, "" );            
        END IF;

        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_institute_designation", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_institute_designation$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_institute_designation$delete AFTER DELETE ON mooc_institute_designation FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'user_id': ", OLD.user_id, "" ); 
        set message = concat( message, ",'institute_id': ", OLD.institute_id, "" );
        set message = concat( message, ",'role_id': ", OLD.role_id, "" );
        set message = concat( message, ",'is_approved': ", OLD.is_approved, "" );           
        set message = concat( message, "}" );
        
        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_institute_designation", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_institute_registration$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_institute_registration$insert AFTER INSERT ON mooc_institute_registration FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'name': '", NEW.name, "'" ); 
        set message = concat( message, ",'state_id': ", NEW.state_id, "" );
        set message = concat( message, ",'city_id': ", NEW.city_id, "" );
        set message = concat( message, ",'pincode': ", NEW.pincode, "" );
        set message = concat( message, ",'address': '", NEW.address, "'" );
        set message = concat( message, ",'website': '", NEW.website, "'" );
        set message = concat( message, ",'is_parent': ", NEW.is_parent, "" );
        set message = concat( message, ",'status_id': ", NEW.status_id, "" );
        set message = concat( message, ",'remarks': '''", IFNULL(NEW.remarks, 'NULL'), "'''" );          
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_institute_registration", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_institute_registration$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_institute_registration$update AFTER UPDATE ON mooc_institute_registration FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 

        IF NEW.name <> OLD.name THEN
            set message = concat( message, ",'name': '", NEW.name, "'" );
        END IF;

        IF NEW.state_id <> OLD.state_id THEN
            set message = concat( message, ",'state_id': ", NEW.state_id, "" ); 
        END IF;

        IF NEW.city_id <> OLD.city_id THEN
            set message = concat( message, ",'city_id': ", NEW.city_id, "" ); 
        END IF;

        IF NEW.pincode <> OLD.pincode THEN
            set message = concat( message, ",'pincode': ", NEW.pincode, "" );            
        END IF;

        IF NEW.address <> OLD.address THEN
            set message = concat( message, ",'address': '", NEW.address, "'" );            
        END IF;

        IF NEW.website <> OLD.website THEN
            set message = concat( message, ",'website': '", NEW.website, "'" );            
        END IF;

        IF NEW.is_parent <> OLD.is_parent THEN
            set message = concat( message, ",'is_parent': ", NEW.is_parent, "" );         
        END IF;

        IF NEW.status_id <> OLD.status_id THEN
            set message = concat( message, ",'status_id': ", NEW.status_id, "" );         
        END IF;

        IF ( (OLD.remarks IS NOT NULL AND NEW.remarks IS NOT NULL 
               AND NEW.remarks <> OLD.remarks) 
               OR (NEW.remarks IS NULL AND OLD.remarks IS NOT NULL) 
               OR (NEW.remarks IS NOT NULL <> OLD.remarks IS NULL) ) THEN
            set message = concat( message, ",'remarks': '''", IFNULL(NEW.remarks, 'NULL'), "'''" );         
        END IF;

        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_institute_registration", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_institute_registration$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_institute_registration$delete AFTER DELETE ON mooc_institute_registration FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'name': '", OLD.name, "'" ); 
        set message = concat( message, ",'state_id': ", OLD.state_id, "" );
        set message = concat( message, ",'city_id': ", OLD.city_id, "" );
        set message = concat( message, ",'pincode': ", OLD.pincode, "" );
        set message = concat( message, ",'address': '", OLD.address, "'" );
        set message = concat( message, ",'website': '", OLD.website, "'" );
        set message = concat( message, ",'is_parent': ", OLD.is_parent, "" );
        set message = concat( message, ",'status_id': ", OLD.status_id, "" );
        set message = concat( message, ",'remarks': '''", IFNULL(OLD.remarks, 'NULL'), "'''" );          
        set message = concat( message, "}" ); 

        
        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_institute_registration", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_institute_status$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_institute_status$insert AFTER INSERT ON mooc_institute_status FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'name': '", NEW.name, "'" );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_institute_status", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_institute_status$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_institute_status$update AFTER UPDATE ON mooc_institute_status FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 


        IF NEW.name <> OLD.name THEN
            set message = concat( message, ",'name': '", NEW.name, "'" ); 
        END IF;


        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_institute_status", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_institute_status$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_institute_status$delete AFTER DELETE ON mooc_institute_status FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'name': '", OLD.name, "'" ); 
        set message = concat( message, "}" );

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_institute_status", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_role$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_role$insert AFTER INSERT ON mooc_role FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'name': '", NEW.name, "'" );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_role", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_role$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_role$update AFTER UPDATE ON mooc_role FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 


        IF NEW.name <> OLD.name THEN
            set message = concat( message, ",'name': '", NEW.name, "'" ); 
        END IF;


        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_role", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_role$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_role$delete AFTER DELETE ON mooc_role FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'name': '", OLD.name, "'" ); 
        set message = concat( message, "}" );

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_role", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_student_institute$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_student_institute$insert AFTER INSERT ON mooc_student_institute FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'user_id': ", NEW.user_id, "" ); 
        set message = concat( message, ",'institute_id': ", NEW.institute_id, "" );
        set message = concat( message, ",'active_from': '''", IFNULL(NEW.active_from, 'NULL'), "'''" );
        set message = concat( message, ",'active_upto': '''", IFNULL(NEW.active_upto, 'NULL'), "'''" );
        set message = concat( message, ",'status_id': ", NEW.status_id, "" );
        set message = concat( message, ",'course_id': ", NEW.course_id, "" );
                   
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_student_institute", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_student_institute$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_student_institute$update AFTER UPDATE ON mooc_student_institute FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 

        IF NEW.user_id <> OLD.user_id THEN
            set message = concat( message, ",'user_id': ", NEW.user_id, "" ); 
        END IF;

        IF NEW.institute_id <> OLD.institute_id THEN
            set message = concat( message, ",'institute_id': ", NEW.institute_id, "" ); 
        END IF;

        IF ( (OLD.active_from IS NOT NULL AND NEW.active_from IS NOT NULL 
               AND NEW.active_from <> OLD.active_from) 
               OR (NEW.active_from IS NULL AND OLD.active_from IS NOT NULL) 
               OR (NEW.active_from IS NOT NULL <> OLD.active_from IS NULL) ) THEN
            set message = concat( message, ",'active_from': '''", IFNULL(NEW.active_from, 'NULL'), "'''" ); 
        END IF;

        IF ( (OLD.active_upto IS NOT NULL AND NEW.active_upto IS NOT NULL 
               AND NEW.active_upto <> OLD.active_upto) 
               OR (NEW.active_upto IS NULL AND OLD.active_upto IS NOT NULL) 
               OR (NEW.active_upto IS NOT NULL <> OLD.active_upto IS NULL) ) THEN
            set message = concat( message, ",'active_upto': '''", IFNULL(NEW.active_upto, 'NULL'), "'''" ); 
        END IF;

        IF NEW.status_id <> OLD.status_id THEN
            set message = concat( message, ",'status_id': ", NEW.status_id, "" ); 
        END IF;

        IF NEW.course_id <> OLD.course_id THEN
            set message = concat( message, ",'course_id': ", NEW.course_id, "" ); 
        END IF;

        
        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_student_institute", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `mooc_student_institute$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER mooc_student_institute$delete AFTER DELETE ON mooc_student_institute FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'user_id': ", OLD.user_id, "" ); 
        set message = concat( message, ",'institute_id': ", OLD.institute_id, "" );
        set message = concat( message, ",'active_from': '''", IFNULL(OLD.active_from, 'NULL'), "'''" );
        set message = concat( message, ",'active_upto': '''", IFNULL(OLD.active_upto, 'NULL'), "'''" );
        set message = concat( message, ",'status_id': ", OLD.status_id, "" );
        set message = concat( message, ",'course_id': ", OLD.course_id, "" );
        set message = concat( message, "}" );
        
        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "mooc_student_institute", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_article$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_article$insert AFTER INSERT ON wiki_article FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'current_revision_id': ", IFNULL(NEW.current_revision_id, '''NULL'''), "" );
        set message = concat( message, ",'created': '", NEW.created, "'" );
        set message = concat( message, ",'modified': '", NEW.modified, "'" );
        set message = concat( message, ",'owner_id': ", IFNULL(NEW.owner_id, '''NULL'''), "" );
        set message = concat( message, ",'group_id': ", IFNULL(NEW.group_id, '''NULL'''), "" );
        set message = concat( message, ",'group_read': ", NEW.group_read, "" );
        set message = concat( message, ",'group_write': ", NEW.group_write, "" );
        set message = concat( message, ",'other_read': ", NEW.other_read, "" );
        set message = concat( message, ",'other_write': ", NEW.other_write, "" );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_article", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_article$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_article$update AFTER UPDATE ON wiki_article FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 
        
        IF ( (OLD.current_revision_id IS NOT NULL AND NEW.current_revision_id IS NOT NULL 
               AND NEW.current_revision_id <> OLD.current_revision_id) 
               OR (NEW.current_revision_id IS NULL AND OLD.current_revision_id IS NOT NULL) 
               OR (NEW.current_revision_id IS NOT NULL <> OLD.current_revision_id IS NULL) ) THEN
            set message = concat( message, ",'current_revision_id': ", IFNULL(NEW.current_revision_id, '''NULL'''), "" );
        END IF;
        
        IF NEW.created <> OLD.created THEN
            set message = concat( message, ",'created': '", NEW.created, "'" ); 
        END IF;

        IF NEW.modified <> OLD.modified THEN
            set message = concat( message, ",'modified': '", NEW.modified, "'" ); 
        END IF;

        IF ( (OLD.owner_id IS NOT NULL AND NEW.owner_id IS NOT NULL 
               AND NEW.owner_id <> OLD.owner_id) 
               OR (NEW.owner_id IS NULL AND OLD.owner_id IS NOT NULL) 
               OR (NEW.owner_id IS NOT NULL <> OLD.owner_id IS NULL) ) THEN
            set message = concat( message, ",'owner_id': ", IFNULL(NEW.owner_id, '''NULL'''), "" ); 
        END IF;


        IF ( (OLD.group_id IS NOT NULL AND NEW.group_id IS NOT NULL 
               AND NEW.group_id <> OLD.group_id) 
               OR (NEW.group_id IS NULL AND OLD.group_id IS NOT NULL) 
               OR (NEW.group_id IS NOT NULL <> OLD.group_id IS NULL) ) THEN
            set message = concat( message, ",'group_id': ", IFNULL(NEW.group_id, '''NULL'''), "" ); 
        END IF;

        IF NEW.group_read <> OLD.group_read THEN
            set message = concat( message, ",'group_read': ", NEW.group_read, "" ); 
        END IF;

        IF NEW.group_write <> OLD.group_write THEN
            set message = concat( message, ",'group_write': ", NEW.group_write, "" ); 
        END IF;

        IF NEW.other_read <> OLD.other_read THEN
            set message = concat( message, ",'other_read': ", NEW.other_read, "" ); 
        END IF;

        IF NEW.other_write <> OLD.other_write THEN
            set message = concat( message, ",'other_write': ", NEW.other_write, "" ); 
        END IF;


        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_article", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_article$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_article$delete AFTER DELETE ON wiki_article FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'current_revision_id': ", IFNULL(OLD.current_revision_id, '''NULL'''), "" );
        set message = concat( message, ",'created': '", OLD.created, "'" );
        set message = concat( message, ",'modified': '", OLD.modified, "'" );
        set message = concat( message, ",'owner_id': ", IFNULL(OLD.owner_id, '''NULL'''), "" );
        set message = concat( message, ",'group_id': ", IFNULL(OLD.group_id, '''NULL'''), "" );
        set message = concat( message, ",'group_read': ", OLD.group_read, "" );
        set message = concat( message, ",'group_write': ", OLD.group_write, "" );
        set message = concat( message, ",'other_read': ", OLD.other_read, "" );
        set message = concat( message, ",'other_write': ", OLD.other_write, "" );
        set message = concat( message, "}" );

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_article", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_articleforobject$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_articleforobject$insert AFTER INSERT ON wiki_articleforobject FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'article_id': ", NEW.article_id, "" ); 
        set message = concat( message, ",'content_type_id': ", NEW.content_type_id, "" );
        set message = concat( message, ",'object_id': ", NEW.object_id, "" );
        set message = concat( message, ",'is_mptt': ", NEW.is_mptt, "" );           
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_articleforobject", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_articleforobject$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_articleforobject$update AFTER UPDATE ON wiki_articleforobject FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 

        IF NEW.article_id <> OLD.article_id THEN
            set message = concat( message, ",'article_id': ", NEW.article_id, "" ); 
        END IF;

        IF NEW.content_type_id <> OLD.content_type_id THEN
            set message = concat( message, ",'content_type_id': ", NEW.content_type_id, "" ); 
        END IF;

        IF NEW.object_id <> OLD.object_id THEN
            set message = concat( message, ",'object_id': ", NEW.object_id, "" ); 
        END IF;

        IF NEW.is_mptt <> OLD.is_mptt THEN
            set message = concat( message, ",'is_mptt': ", NEW.is_mptt, "" );            
        END IF;

        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_articleforobject", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_articleforobject$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_articleforobject$delete AFTER DELETE ON wiki_articleforobject FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'article_id': ", OLD.article_id, "" ); 
        set message = concat( message, ",'content_type_id': ", OLD.content_type_id, "" );
        set message = concat( message, ",'object_id': ", OLD.object_id, "" );
        set message = concat( message, ",'is_mptt': ", OLD.is_mptt, "" );           
        set message = concat( message, "}" );
        
        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_articleforobject", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_articleplugin$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_articleplugin$insert AFTER INSERT ON wiki_articleplugin FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'article_id': ", NEW.article_id, "" );
        set message = concat( message, ",'deleted': ", NEW.deleted, "" );
        set message = concat( message, ",'created': '", NEW.created, "'" );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_articleplugin", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_articleplugin$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_articleplugin$update AFTER UPDATE ON wiki_articleplugin FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 
        
        IF NEW.article_id <> OLD.article_id THEN
            set message = concat( message, ",'article_id': ", NEW.article_id, "" ); 
        END IF;

        IF NEW.deleted <> OLD.deleted THEN
            set message = concat( message, ",'deleted': ", NEW.deleted, "" ); 
        END IF;

        IF NEW.created <> OLD.created THEN
            set message = concat( message, ",'created': '", NEW.created, "'" ); 
        END IF;

        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_articleplugin", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_articleplugin$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_articleplugin$delete AFTER DELETE ON wiki_articleplugin FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'article_id': ", OLD.article_id, "" );
        set message = concat( message, ",'deleted': ", OLD.deleted, "" );
        set message = concat( message, ",'created': '", OLD.created, "'" );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_articleplugin", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_articlerevision$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_articlerevision$insert AFTER INSERT ON wiki_articlerevision FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'revision_number': ", NEW.revision_number, "" );
        set message = concat( message, ",'user_message': '''", NEW.user_message, "'''" );
        set message = concat( message, ",'automatic_log': '''", NEW.automatic_log, "'''" );
        set message = concat( message, ",'ip_address': '''", IFNULL(NEW.ip_address, 'NULL'), "'''" );
        set message = concat( message, ",'user_id': ", IFNULL(NEW.user_id, '''NULL'''), "" );
        set message = concat( message, ",'modified': '", NEW.modified, "'" ); 
        set message = concat( message, ",'created': '", NEW.created, "'" ); 
        set message = concat( message, ",'previous_revision_id': ", IFNULL(NEW.previous_revision_id, '''NULL'''), "" );
        set message = concat( message, ",'deleted': ", NEW.deleted, "" );
        set message = concat( message, ",'locked': ", NEW.locked, "" );
        set message = concat( message, ",'article_id': ", NEW.article_id, "" );
        set message = concat( message, ",'content': '''", NEW.content, "'''" );
        set message = concat( message, ",'title': '''", NEW.title, "'''" );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_articlerevision", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_articlerevision$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_articlerevision$update AFTER UPDATE ON wiki_articlerevision FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 

        IF NEW.revision_number <> OLD.revision_number THEN
            set message = concat( message, ",'revision_number': ", NEW.revision_number, "" ); 
        END IF;

        IF NEW.user_message <> OLD.user_message THEN
            set message = concat( message, ",'user_message': '''", NEW.user_message, "'''" ); 
        END IF;

        IF NEW.automatic_log <> OLD.automatic_log THEN
            set message = concat( message, ",'automatic_log': '''", NEW.automatic_log, "'''" );
        END IF;

        IF ( (OLD.ip_address IS NOT NULL AND NEW.ip_address IS NOT NULL 
               AND NEW.ip_address <> OLD.ip_address) 
               OR (NEW.ip_address IS NULL AND OLD.ip_address IS NOT NULL) 
               OR (NEW.ip_address IS NOT NULL <> OLD.ip_address IS NULL) ) THEN
            set message = concat( message, ",'ip_address': '''", IFNULL(NEW.ip_address, 'NULL'), "'''" );
        END IF;

        IF ( (OLD.user_id IS NOT NULL AND NEW.user_id IS NOT NULL 
               AND NEW.user_id <> OLD.user_id) 
               OR (NEW.user_id IS NULL AND OLD.user_id IS NOT NULL) 
               OR (NEW.user_id IS NOT NULL <> OLD.user_id IS NULL) ) THEN
            set message = concat( message, ",'user_id': ", IFNULL(NEW.user_id, '''NULL'''), "" ); 
        END IF;

        IF NEW.modified <> OLD.modified THEN
            set message = concat( message, ",'modified': '", NEW.modified, "'" );
        END IF;

        IF NEW.created <> OLD.created THEN
            set message = concat( message, ",'created': '", NEW.created, "'" ); 
        END IF;

        IF ( (OLD.previous_revision_id IS NOT NULL AND NEW.previous_revision_id IS NOT NULL 
               AND NEW.previous_revision_id <> OLD.previous_revision_id) 
               OR (NEW.previous_revision_id IS NULL AND OLD.previous_revision_id IS NOT NULL) 
               OR (NEW.previous_revision_id IS NOT NULL <> OLD.previous_revision_id IS NULL) ) THEN
            set message = concat( message, ",'previous_revision_id': ", IFNULL(NEW.previous_revision_id, '''NULL'''), "" ); 
        END IF;

        IF NEW.deleted <> OLD.deleted THEN
            set message = concat( message, ",'deleted': ", NEW.deleted, "" ); 
        END IF;

        IF NEW.locked <> OLD.locked THEN
            set message = concat( message, ",'locked': ", NEW.locked, "" ); 
        END IF;

        IF NEW.article_id <> OLD.article_id THEN
            set message = concat( message, ",'article_id': ", NEW.article_id, "" );
        END IF;

        IF NEW.content <> OLD.content THEN
            set message = concat( message, ",'content': '''", NEW.content, "'''" ); 
        END IF;

        IF NEW.title <> OLD.title THEN
            set message = concat( message, ",'title': '''", NEW.title, "'''" ); 
        END IF;

        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_articlerevision", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_articlerevision$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_articlerevision$delete AFTER DELETE ON wiki_articlerevision FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'revision_number': ", OLD.revision_number, "" );
        set message = concat( message, ",'user_message': '''", OLD.user_message, "'''" );
        set message = concat( message, ",'automatic_log': '''", OLD.automatic_log, "'''" );
        set message = concat( message, ",'ip_address': '''", IFNULL(OLD.ip_address, 'NULL'), "'''" );
        set message = concat( message, ",'user_id': ", IFNULL(OLD.user_id, '''NULL'''), "" );
        set message = concat( message, ",'modified': '", OLD.modified, "'" ); 
        set message = concat( message, ",'created': '", OLD.created, "'" ); 
        set message = concat( message, ",'previous_revision_id': ", IFNULL(OLD.previous_revision_id, '''NULL'''), "" );
        set message = concat( message, ",'deleted': ", OLD.deleted, "" );
        set message = concat( message, ",'locked': ", OLD.locked, "" );
        set message = concat( message, ",'article_id': ", OLD.article_id, "" );
        set message = concat( message, ",'content': '''", OLD.content, "'''" );
        set message = concat( message, ",'title': '''", OLD.title, "'''" );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_articlerevision", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_articlesubscription$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_articlesubscription$insert AFTER INSERT ON wiki_articlesubscription FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'articleplugin_ptr_id': ", NEW.articleplugin_ptr_id );
        set message = concat( message, ",'subscription_ptr_id': ", NEW.subscription_ptr_id, "" );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_articlesubscription", "INSERT", NEW.articleplugin_ptr_id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_articlesubscription$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_articlesubscription$update AFTER UPDATE ON wiki_articlesubscription FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'articleplugin_ptr_id': ", NEW.articleplugin_ptr_id );
        set no_message = concat( no_message, "{'articleplugin_ptr_id': ", NEW.articleplugin_ptr_id ); 
        
        IF NEW.subscription_ptr_id <> OLD.subscription_ptr_id THEN
            set message = concat( message, ",'subscription_ptr_id': ", NEW.subscription_ptr_id, "" ); 
        END IF;


        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_articlesubscription", "UPDATE", NEW.articleplugin_ptr_id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_articlesubscription$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_articlesubscription$delete AFTER DELETE ON wiki_articlesubscription FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'articleplugin_ptr_id': ", OLD.articleplugin_ptr_id );
        set message = concat( message, ",'subscription_ptr_id': ", OLD.subscription_ptr_id, "" );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_articlesubscription", "DELETE", OLD.articleplugin_ptr_id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_attachment$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_attachment$insert AFTER INSERT ON wiki_attachment FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'reusableplugin_ptr_id': ", NEW.reusableplugin_ptr_id );
        set message = concat( message, ",'current_revision_id': ", IFNULL(NEW.current_revision_id, '''NULL'''), "" );
        set message = concat( message, ",'original_filename': '''", IFNULL(NEW.original_filename, 'NULL'), "'''" ); 
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_attachment", "INSERT", NEW.reusableplugin_ptr_id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_attachment$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_attachment$update AFTER UPDATE ON wiki_attachment FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'reusableplugin_ptr_id': ", NEW.reusableplugin_ptr_id );
        set no_message = concat( no_message, "{'reusableplugin_ptr_id': ", NEW.reusableplugin_ptr_id ); 
        
        IF ( (OLD.current_revision_id IS NOT NULL AND NEW.current_revision_id IS NOT NULL 
               AND NEW.current_revision_id <> OLD.current_revision_id) 
               OR (NEW.current_revision_id IS NULL AND OLD.current_revision_id IS NOT NULL) 
               OR (NEW.current_revision_id IS NOT NULL <> OLD.current_revision_id IS NULL) ) THEN
            set message = concat( message, ",'current_revision_id': ", IFNULL(NEW.current_revision_id, '''NULL'''), "" ); 
        END IF;

        IF ( (OLD.original_filename IS NOT NULL AND NEW.original_filename IS NOT NULL 
               AND NEW.original_filename <> OLD.original_filename) 
               OR (NEW.original_filename IS NULL AND OLD.original_filename IS NOT NULL) 
               OR (NEW.original_filename IS NOT NULL <> OLD.original_filename IS NULL) ) THEN
            set message = concat( message, ",'original_filename': '''", IFNULL(NEW.original_filename, 'NULL'), "'''" ); 
        END IF;


        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_attachment", "UPDATE", NEW.reusableplugin_ptr_id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_attachment$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_attachment$delete AFTER DELETE ON wiki_attachment FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'reusableplugin_ptr_id': ", OLD.reusableplugin_ptr_id );
        set message = concat( message, ",'current_revision_id': ", IFNULL(OLD.current_revision_id, '''NULL'''), "" );
        set message = concat( message, ",'original_filename': '''", IFNULL(OLD.original_filename, 'NULL'), "'''" ); 
        set message = concat( message, "}" );
 
        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_attachment", "DELETE", OLD.reusableplugin_ptr_id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_attachmentrevision$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_attachmentrevision$insert AFTER INSERT ON wiki_attachmentrevision FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'revision_number': ", NEW.revision_number, "" );
        set message = concat( message, ",'user_message': '''", NEW.user_message, "'''" );
        set message = concat( message, ",'automatic_log': '''", NEW.automatic_log, "'''" );
        set message = concat( message, ",'ip_address': '''", IFNULL(NEW.ip_address, 'NULL'), "'''" );
        set message = concat( message, ",'user_id': ", IFNULL(NEW.user_id, '''NULL'''), "" );
        set message = concat( message, ",'modified': '", NEW.modified, "'" ); 
        set message = concat( message, ",'created': '", NEW.created, "'" ); 
        set message = concat( message, ",'previous_revision_id': ", IFNULL(NEW.previous_revision_id, '''NULL'''), "" );
        set message = concat( message, ",'deleted': ", NEW.deleted, "" );
        set message = concat( message, ",'locked': ", NEW.locked, "" );
        set message = concat( message, ",'attachment_id': ", NEW.attachment_id, "" );
        set message = concat( message, ",'file': '''", NEW.file, "'''" );
        set message = concat( message, ",'description': '''", NEW.description, "'''" );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_attachmentrevision", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_attachmentrevision$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_attachmentrevision$update AFTER UPDATE ON wiki_attachmentrevision FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 

        IF NEW.revision_number <> OLD.revision_number THEN
            set message = concat( message, ",'revision_number': ", NEW.revision_number, "" ); 
        END IF;

        IF NEW.user_message <> OLD.user_message THEN
            set message = concat( message, ",'user_message': '''", NEW.user_message, "'''" ); 
        END IF;

        IF NEW.automatic_log <> OLD.automatic_log THEN
            set message = concat( message, ",'automatic_log': '''", NEW.automatic_log, "'''" );
        END IF;

        IF ( (OLD.ip_address IS NOT NULL AND NEW.ip_address IS NOT NULL 
               AND NEW.ip_address <> OLD.ip_address) 
               OR (NEW.ip_address IS NULL AND OLD.ip_address IS NOT NULL) 
               OR (NEW.ip_address IS NOT NULL <> OLD.ip_address IS NULL) ) THEN
            set message = concat( message, ",'ip_address': '''", IFNULL(NEW.ip_address, 'NULL'), "'''" );
        END IF;

        IF ( (OLD.user_id IS NOT NULL AND NEW.user_id IS NOT NULL 
               AND NEW.user_id <> OLD.user_id) 
               OR (NEW.user_id IS NULL AND OLD.user_id IS NOT NULL) 
               OR (NEW.user_id IS NOT NULL <> OLD.user_id IS NULL) ) THEN
            set message = concat( message, ",'user_id': ", IFNULL(NEW.user_id, '''NULL'''), "" ); 
        END IF;

        IF NEW.modified <> OLD.modified THEN
            set message = concat( message, ",'modified': '", NEW.modified, "'" );
        END IF;

        IF NEW.created <> OLD.created THEN
            set message = concat( message, ",'created': '", NEW.created, "'" ); 
        END IF;

        IF ( (OLD.previous_revision_id IS NOT NULL AND NEW.previous_revision_id IS NOT NULL 
               AND NEW.previous_revision_id <> OLD.previous_revision_id) 
               OR (NEW.previous_revision_id IS NULL AND OLD.previous_revision_id IS NOT NULL) 
               OR (NEW.previous_revision_id IS NOT NULL <> OLD.previous_revision_id IS NULL) ) THEN
            set message = concat( message, ",'previous_revision_id': ", IFNULL(NEW.previous_revision_id, '''NULL'''), "" ); 
        END IF;

        IF NEW.deleted <> OLD.deleted THEN
            set message = concat( message, ",'deleted': ", NEW.deleted, "" ); 
        END IF;

        IF NEW.locked <> OLD.locked THEN
            set message = concat( message, ",'locked': ", NEW.locked, "" ); 
        END IF;

        IF NEW.attachment_id <> OLD.attachment_id THEN
            set message = concat( message, ",'attachment_id': ", NEW.attachment_id, "" );
        END IF;

        IF NEW.file <> OLD.file THEN
            set message = concat( message, ",'file': '''", NEW.file, "'''" );
        END IF;

        IF NEW.description <> OLD.description THEN
            set message = concat( message, ",'description': '''", NEW.description, "'''" ); 
        END IF;

        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_attachmentrevision", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_attachmentrevision$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_attachmentrevision$delete AFTER DELETE ON wiki_attachmentrevision FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'revision_number': ", OLD.revision_number, "" );
        set message = concat( message, ",'user_message': '''", OLD.user_message, "'''" );
        set message = concat( message, ",'automatic_log': '''", OLD.automatic_log, "'''" );
        set message = concat( message, ",'ip_address': '''", IFNULL(OLD.ip_address, 'NULL'), "'''" );
        set message = concat( message, ",'user_id': ", IFNULL(OLD.user_id, '''NULL'''), "" );
        set message = concat( message, ",'modified': '", OLD.modified, "'" ); 
        set message = concat( message, ",'created': '", OLD.created, "'" ); 
        set message = concat( message, ",'previous_revision_id': ", IFNULL(OLD.previous_revision_id, '''NULL'''), "" );
        set message = concat( message, ",'deleted': ", OLD.deleted, "" );
        set message = concat( message, ",'locked': ", OLD.locked, "" );
        set message = concat( message, ",'attachment_id': ", OLD.attachment_id, "" );
        set message = concat( message, ",'file': '''", OLD.file, "'''" );
        set message = concat( message, ",'description': '''", OLD.description, "'''" );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_attachmentrevision", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_image$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_image$insert AFTER INSERT ON wiki_image FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'revisionplugin_ptr_id': ", NEW.revisionplugin_ptr_id );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_image", "INSERT", NEW.revisionplugin_ptr_id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_image$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_image$update AFTER UPDATE ON wiki_image FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'revisionplugin_ptr_id': ", NEW.revisionplugin_ptr_id );
        set no_message = concat( no_message, "{'revisionplugin_ptr_id': ", NEW.revisionplugin_ptr_id ); 
        
        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_image", "UPDATE", NEW.revisionplugin_ptr_id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_image$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_image$delete AFTER DELETE ON wiki_image FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'revisionplugin_ptr_id': ", OLD.revisionplugin_ptr_id );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_image", "DELETE", OLD.revisionplugin_ptr_id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_imagerevision$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_imagerevision$insert AFTER INSERT ON wiki_imagerevision FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'revisionpluginrevision_ptr_id': ", NEW.revisionpluginrevision_ptr_id );
        set message = concat( message, ",'image': '''", IFNULL(NEW.image, 'NULL'), "'''" ); 
        set message = concat( message, ",'width': ", IFNULL(NEW.width, '''NULL'''), "" );
        set message = concat( message, ",'height': ", IFNULL(NEW.height, '''NULL'''), "" );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_imagerevision", "INSERT", NEW.revisionpluginrevision_ptr_id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_imagerevision$update`;

DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_imagerevision$update AFTER UPDATE ON wiki_imagerevision FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'revisionpluginrevision_ptr_id': ", NEW.revisionpluginrevision_ptr_id );
        set no_message = concat( no_message, "{'revisionpluginrevision_ptr_id': ", NEW.revisionpluginrevision_ptr_id ); 
        
        IF ( (OLD.image IS NOT NULL AND NEW.image IS NOT NULL 
               AND NEW.image <> OLD.image) 
               OR (NEW.image IS NULL AND OLD.image IS NOT NULL) 
               OR (NEW.image IS NOT NULL <> OLD.image IS NULL) ) THEN
            set message = concat( message, ",'image': '''", IFNULL(NEW.image, 'NULL'), "'''" ); 
        END IF;

        IF ( (OLD.width IS NOT NULL AND NEW.width IS NOT NULL 
               AND NEW.width <> OLD.width) 
               OR (NEW.width IS NULL AND OLD.width IS NOT NULL) 
               OR (NEW.width IS NOT NULL <> OLD.width IS NULL) ) THEN
            set message = concat( message, ",'width': ", IFNULL(NEW.width, '''NULL'''), "" );
        END IF;

        IF ( (OLD.height IS NOT NULL AND NEW.height IS NOT NULL 
               AND NEW.height <> OLD.height) 
               OR (NEW.height IS NULL AND OLD.height IS NOT NULL) 
               OR (NEW.height IS NOT NULL <> OLD.height IS NULL) ) THEN
            set message = concat( message, ",'height': ", IFNULL(NEW.height, '''NULL'''), "" );
        END IF;

        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_imagerevision", "UPDATE", NEW.revisionpluginrevision_ptr_id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_imagerevision$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_imagerevision$delete AFTER DELETE ON wiki_imagerevision FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'revisionpluginrevision_ptr_id': ", OLD.revisionpluginrevision_ptr_id );
        set message = concat( message, ",'image': '''", IFNULL(OLD.image, 'NULL'), "'''" ); 
        set message = concat( message, ",'width': ", IFNULL(OLD.width, '''NULL'''), "" );
        set message = concat( message, ",'height': ", IFNULL(OLD.height, '''NULL'''), "" );
        set message = concat( message, "}" );
 
        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_imagerevision", "DELETE", OLD.revisionpluginrevision_ptr_id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_reusableplugin$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_reusableplugin$insert AFTER INSERT ON wiki_reusableplugin FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'articleplugin_ptr_id': ", NEW.articleplugin_ptr_id );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_reusableplugin", "INSERT", NEW.articleplugin_ptr_id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_reusableplugin$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_reusableplugin$update AFTER UPDATE ON wiki_reusableplugin FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'articleplugin_ptr_id': ", NEW.articleplugin_ptr_id );
        set no_message = concat( no_message, "{'articleplugin_ptr_id': ", NEW.articleplugin_ptr_id ); 
        
        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_reusableplugin", "UPDATE", NEW.articleplugin_ptr_id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_reusableplugin$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_reusableplugin$delete AFTER DELETE ON wiki_reusableplugin FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'articleplugin_ptr_id': ", OLD.articleplugin_ptr_id );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_reusableplugin", "DELETE", OLD.articleplugin_ptr_id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_reusableplugin_articles$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_reusableplugin_articles$insert AFTER INSERT ON wiki_reusableplugin_articles FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'reusableplugin_id': ", NEW.reusableplugin_id, "" );
        set message = concat( message, ",'article_id': ", NEW.article_id, "" ); 
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_reusableplugin_articles", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_reusableplugin_articles$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_reusableplugin_articles$update AFTER UPDATE ON wiki_reusableplugin_articles FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 

        IF NEW.reusableplugin_id <> OLD.reusableplugin_id THEN
            set message = concat( message, ",'reusableplugin_id': ", NEW.reusableplugin_id, "" ); 
        END IF;

        IF NEW.article_id <> OLD.article_id THEN
            set message = concat( message, ",'article_id': ", NEW.article_id, "" ); 
        END IF;

        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_reusableplugin_articles", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_reusableplugin_articles$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_reusableplugin_articles$delete AFTER DELETE ON wiki_reusableplugin_articles FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'reusableplugin_id': ", OLD.reusableplugin_id, "" );
        set message = concat( message, ",'article_id': ", OLD.article_id, "" ); 
        set message = concat( message, "}" );
        
        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_reusableplugin_articles", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_revisionplugin$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_revisionplugin$insert AFTER INSERT ON wiki_revisionplugin FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'articleplugin_ptr_id': ", NEW.articleplugin_ptr_id );
        set message = concat( message, ",'current_revision_id': ", IFNULL(NEW.current_revision_id, '''NULL'''), "" );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_revisionplugin", "INSERT", NEW.articleplugin_ptr_id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_revisionplugin$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_revisionplugin$update AFTER UPDATE ON wiki_revisionplugin FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'articleplugin_ptr_id': ", NEW.articleplugin_ptr_id );
        set no_message = concat( no_message, "{'articleplugin_ptr_id': ", NEW.articleplugin_ptr_id ); 
        
        IF ( (OLD.current_revision_id IS NOT NULL AND NEW.current_revision_id IS NOT NULL 
               AND NEW.current_revision_id <> OLD.current_revision_id) 
               OR (NEW.current_revision_id IS NULL AND OLD.current_revision_id IS NOT NULL) 
               OR (NEW.current_revision_id IS NOT NULL <> OLD.current_revision_id IS NULL) ) THEN
            set message = concat( message, ",'current_revision_id': ", IFNULL(NEW.current_revision_id, '''NULL'''), "" ); 
        END IF;


        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_revisionplugin", "UPDATE", NEW.articleplugin_ptr_id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_revisionplugin$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_revisionplugin$delete AFTER DELETE ON wiki_revisionplugin FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'articleplugin_ptr_id': ", OLD.articleplugin_ptr_id );
        set message = concat( message, ",'current_revision_id': ", IFNULL(OLD.current_revision_id, '''NULL'''), "" );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_revisionplugin", "DELETE", OLD.articleplugin_ptr_id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_revisionpluginrevision$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_revisionpluginrevision$insert AFTER INSERT ON wiki_revisionpluginrevision FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'revision_number': ", NEW.revision_number, "" );
        set message = concat( message, ",'user_message': '''", NEW.user_message, "'''" );
        set message = concat( message, ",'automatic_log': '''", NEW.automatic_log, "'''" );
        set message = concat( message, ",'ip_address': '''", IFNULL(NEW.ip_address, 'NULL'), "'''" );
        set message = concat( message, ",'user_id': ", IFNULL(NEW.user_id, '''NULL'''), "" );
        set message = concat( message, ",'modified': '", NEW.modified, "'" ); 
        set message = concat( message, ",'created': '", NEW.created, "'" ); 
        set message = concat( message, ",'previous_revision_id': ", IFNULL(NEW.previous_revision_id, '''NULL'''), "" );
        set message = concat( message, ",'deleted': ", NEW.deleted, "" );
        set message = concat( message, ",'locked': ", NEW.locked, "" );
        set message = concat( message, ",'plugin_id': ", NEW.plugin_id, "" );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_revisionpluginrevision", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_revisionpluginrevision$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_revisionpluginrevision$update AFTER UPDATE ON wiki_revisionpluginrevision FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 

        IF NEW.revision_number <> OLD.revision_number THEN
            set message = concat( message, ",'revision_number': ", NEW.revision_number, "" ); 
        END IF;

        IF NEW.user_message <> OLD.user_message THEN
            set message = concat( message, ",'user_message': '''", NEW.user_message, "'''" ); 
        END IF;

        IF NEW.automatic_log <> OLD.automatic_log THEN
            set message = concat( message, ",'automatic_log': '''", NEW.automatic_log, "'''" );
        END IF;

        IF ( (OLD.ip_address IS NOT NULL AND NEW.ip_address IS NOT NULL 
               AND NEW.ip_address <> OLD.ip_address) 
               OR (NEW.ip_address IS NULL AND OLD.ip_address IS NOT NULL) 
               OR (NEW.ip_address IS NOT NULL <> OLD.ip_address IS NULL) ) THEN
            set message = concat( message, ",'ip_address': '''", IFNULL(NEW.ip_address, 'NULL'), "'''" );
        END IF;

        IF ( (OLD.user_id IS NOT NULL AND NEW.user_id IS NOT NULL 
               AND NEW.user_id <> OLD.user_id) 
               OR (NEW.user_id IS NULL AND OLD.user_id IS NOT NULL) 
               OR (NEW.user_id IS NOT NULL <> OLD.user_id IS NULL) ) THEN
            set message = concat( message, ",'user_id': ", IFNULL(NEW.user_id, '''NULL'''), "" ); 
        END IF;

        IF NEW.modified <> OLD.modified THEN
            set message = concat( message, ",'modified': '", NEW.modified, "'" );
        END IF;

        IF NEW.created <> OLD.created THEN
            set message = concat( message, ",'created': '", NEW.created, "'" ); 
        END IF;

        IF ( (OLD.previous_revision_id IS NOT NULL AND NEW.previous_revision_id IS NOT NULL 
               AND NEW.previous_revision_id <> OLD.previous_revision_id) 
               OR (NEW.previous_revision_id IS NULL AND OLD.previous_revision_id IS NOT NULL) 
               OR (NEW.previous_revision_id IS NOT NULL <> OLD.previous_revision_id IS NULL) ) THEN
            set message = concat( message, ",'previous_revision_id': ", IFNULL(NEW.previous_revision_id, '''NULL'''), "" ); 
        END IF;

        IF NEW.deleted <> OLD.deleted THEN
            set message = concat( message, ",'deleted': ", NEW.deleted, "" ); 
        END IF;

        IF NEW.locked <> OLD.locked THEN
            set message = concat( message, ",'locked': ", NEW.locked, "" ); 
        END IF;

        IF NEW.plugin_id <> OLD.plugin_id THEN
            set message = concat( message, ",'plugin_id': ", NEW.plugin_id, "" );
        END IF;

        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_revisionpluginrevision", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_revisionpluginrevision$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_revisionpluginrevision$delete AFTER DELETE ON wiki_revisionpluginrevision FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'revision_number': ", OLD.revision_number, "" );
        set message = concat( message, ",'user_message': '''", OLD.user_message, "'''" );
        set message = concat( message, ",'automatic_log': '''", OLD.automatic_log, "'''" );
        set message = concat( message, ",'ip_address': '''", IFNULL(OLD.ip_address, 'NULL'), "'''" );
        set message = concat( message, ",'user_id': ", IFNULL(OLD.user_id, '''NULL'''), "" );
        set message = concat( message, ",'modified': '", OLD.modified, "'" ); 
        set message = concat( message, ",'created': '", OLD.created, "'" ); 
        set message = concat( message, ",'previous_revision_id': ", IFNULL(OLD.previous_revision_id, '''NULL'''), "" );
        set message = concat( message, ",'deleted': ", OLD.deleted, "" );
        set message = concat( message, ",'locked': ", OLD.locked, "" );
        set message = concat( message, ",'plugin_id': ", OLD.plugin_id, "" );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_revisionpluginrevision", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_simpleplugin$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_simpleplugin$insert AFTER INSERT ON wiki_simpleplugin FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'articleplugin_ptr_id': ", NEW.articleplugin_ptr_id );
        set message = concat( message, ",'article_revision_id': ", NEW.article_revision_id, "" );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_simpleplugin", "INSERT", NEW.articleplugin_ptr_id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_simpleplugin$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_simpleplugin$update AFTER UPDATE ON wiki_simpleplugin FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'articleplugin_ptr_id': ", NEW.articleplugin_ptr_id );
        set no_message = concat( no_message, "{'articleplugin_ptr_id': ", NEW.articleplugin_ptr_id ); 
        
        IF NEW.article_revision_id <> OLD.article_revision_id THEN
            set message = concat( message, ",'article_revision_id': ", NEW.article_revision_id, "" ); 
        END IF;

        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_simpleplugin", "UPDATE", NEW.articleplugin_ptr_id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_simpleplugin$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_simpleplugin$delete AFTER DELETE ON wiki_simpleplugin FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'articleplugin_ptr_id': ", OLD.articleplugin_ptr_id );
        set message = concat( message, ",'article_revision_id': ", OLD.article_revision_id, "" );
        set message = concat( message, "}" );
 
        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_simpleplugin", "DELETE", OLD.articleplugin_ptr_id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_urlpath$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_urlpath$insert AFTER INSERT ON wiki_urlpath FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'slug': '''", IFNULL(NEW.slug, 'NULL'), "'''" );
        set message = concat( message, ",'site_id': ", NEW.site_id, "" ); 
        set message = concat( message, ",'parent_id': ", IFNULL(NEW.parent_id, '''NULL'''), "" );
        set message = concat( message, ",'lft': ", NEW.lft, "" );
        set message = concat( message, ",'rght': ", NEW.rght, "" );
        set message = concat( message, ",'tree_id': ", NEW.tree_id, "" );
        set message = concat( message, ",'level': ", NEW.level, "" );
        set message = concat( message, ",'article_id': ", NEW.article_id, "" ); 
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_urlpath", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_urlpath$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_urlpath$update AFTER UPDATE ON wiki_urlpath FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 

        IF ( (OLD.slug IS NOT NULL AND NEW.slug IS NOT NULL 
               AND NEW.slug <> OLD.slug) 
               OR (NEW.slug IS NULL AND OLD.slug IS NOT NULL) 
               OR (NEW.slug IS NOT NULL <> OLD.slug IS NULL) ) THEN
            set message = concat( message, ",'slug': '''", IFNULL(NEW.slug, 'NULL'), "'''" );
        END IF;

        IF NEW.site_id <> OLD.site_id THEN
            set message = concat( message, ",'site_id': ", NEW.site_id, "" ); 
        END IF;

        IF ( (OLD.parent_id IS NOT NULL AND NEW.parent_id IS NOT NULL 
               AND NEW.parent_id <> OLD.parent_id) 
               OR (NEW.parent_id IS NULL AND OLD.parent_id IS NOT NULL) 
               OR (NEW.parent_id IS NOT NULL <> OLD.parent_id IS NULL) ) THEN
            set message = concat( message, ",'parent_id': ", IFNULL(NEW.parent_id, '''NULL'''), "" );
        END IF;

        IF NEW.lft <> OLD.lft THEN
            set message = concat( message, ",'lft': ", NEW.lft, "" );
        END IF;

        IF NEW.rght <> OLD.rght THEN
            set message = concat( message, ",'rght': ", NEW.rght, "" );
        END IF;

        IF NEW.tree_id <> OLD.tree_id THEN
            set message = concat( message, ",'tree_id': ", NEW.tree_id, "" );
        END IF;

        IF NEW.level <> OLD.level THEN
            set message = concat( message, ",'level': ", NEW.level, "" );
        END IF;

        IF NEW.article_id <> OLD.article_id THEN
            set message = concat( message, ",'article_id': ", NEW.article_id, "" );
        END IF;

        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_urlpath", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `wiki_urlpath$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER wiki_urlpath$delete AFTER DELETE ON wiki_urlpath FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'slug': '''", IFNULL(OLD.slug, 'NULL'), "'''" );
        set message = concat( message, ",'site_id': ", OLD.site_id, "" ); 
        set message = concat( message, ",'parent_id': ", IFNULL(OLD.parent_id, '''NULL'''), "" );
        set message = concat( message, ",'lft': ", OLD.lft, "" );
        set message = concat( message, ",'rght': ", OLD.rght, "" );
        set message = concat( message, ",'tree_id': ", OLD.tree_id, "" );
        set message = concat( message, ",'level': ", OLD.level, "" );
        set message = concat( message, ",'article_id': ", OLD.article_id, "" ); 
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "wiki_urlpath", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `course_creators_coursecreator$insert`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER course_creators_coursecreator$insert AFTER INSERT ON course_creators_coursecreator FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set message = concat( message, ",'user_id': ", NEW.user_id, "" ); 
        set message = concat( message, ",'state_changed': '", NEW.state_changed, "'" ); 
        set message = concat( message, ",'state': '", NEW.state, "'" );
        set message = concat( message, ",'note': '", NEW.note, "'" );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "course_creators_coursecreator", "INSERT", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `course_creators_coursecreator$update`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER course_creators_coursecreator$update AFTER UPDATE ON course_creators_coursecreator FOR EACH ROW 
    BEGIN
        declare no_message varchar(1024) default "";
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", NEW.id );
        set no_message = concat( no_message, "{'id': ", NEW.id ); 


        IF NEW.user_id <> OLD.user_id THEN
            set message = concat( message, ",'user_id': ", NEW.user_id, "" ); 
        END IF;

        IF NEW.state_changed <> OLD.state_changed THEN
            set message = concat( message, ",'state_changed': '", NEW.state_changed, "'" ); 
        END IF;

        IF NEW.state <> OLD.state THEN
            set message = concat( message, ",'state': '", NEW.state, "'" ); 
        END IF;

        IF NEW.note <> OLD.note THEN
            set message = concat( message, ",'note': '", NEW.note, "'" ); 
        END IF;

        set message = concat( message, "}" ); 
        set no_message = concat( no_message, "}" );

        IF no_message <> message THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "course_creators_coursecreator", "UPDATE", NEW.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DROP TRIGGER IF EXISTS `course_creators_coursecreator$delete`;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER course_creators_coursecreator$delete AFTER DELETE ON course_creators_coursecreator FOR EACH ROW 
    BEGIN
        declare message varchar(1024) default "";

        set message = concat( message, "{'id': ", OLD.id );
        set message = concat( message, ",'user_id': ", OLD.user_id, "" ); 
        set message = concat( message, ",'state_changed': '", OLD.state_changed, "'" ); 
        set message = concat( message, ",'state': '", OLD.state, "'" );
        set message = concat( message, ",'note': '", OLD.note, "'" );
        set message = concat( message, "}" ); 

        IF LENGTH(message) > 0 THEN   
            INSERT INTO mooc_course_event (eventbase,timestamp,basename,basetable,baseact,identify,data) VALUES("MYSQL", UTC_TIMESTAMP(), "edxapp", "course_creators_coursecreator", "DELETE", OLD.id, message); 
        END IF;
    END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- completed on 2015-04-27
